# MDSplus LabVIEW

An MDSplus interface for LabVIEW that was developed by Tokamak Energy. This library wraps the mdsip.dll 

This library is presented as-is, with no promise of future support or development.

This library has been tested with MDSplus-7.7-15

## Installation

Either you can build the VIPackage from source, or download the latest from LAVA

*TODO add link to download

## Examples

Once the package is installed, examples can be found in

%LabVIEW%/examples/Tokamak Energy/MDSplus


# Licence

This library inherits the [BSD](http://www.mdsplus.org/index.php/BSD_license) licence from the MDSplus project. 

See also: [Licence](/LICENSE)